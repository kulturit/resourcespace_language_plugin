<?php
# Norwegian
# Language File Plugin for MuseumsIT
# -------
# Note: when translating to a new language, preserve the original case if possible.


# Image size names (for the default image sizes)
$lang["imagesize-collection"]="Mappe";

# Top navigation bar (also reused for page titles)
# next line
$lang["mycollections"]="Mine mapper";
$lang["collections"]="Mappe";

# Team Centre - Resource management
$lang["viewuserpending"]="Gjennomgå brukerbidrag med status: Venter på godkjenning";
$lang["viewuserpendingsubmission"]="Gjennomgå brukerbidrag med status: Venter på brukerendring";

# User contributions
$lang["contributenewresource"]="Registrer og last opp nytt materiale";
$lang["viewcontributedps"]="Vis mine brukerbidrag med Status: Venter på brukerendring";
$lang["viewcontributedpr"]="Vis mine brukerbidrag med Status: Venter på godkjenning";
$lang["viewcontributedsubittedl"]="Vis mine brukerbidrag med Status: Ferdigbehandlet";
$lang["contributedps"]="Mine brukerbidrag - Venter på brukerendring";
$lang["contributedpr"]="Mine brukerbidrag - Venter på godkjenning";
$lang["contributedsubittedl"]="Mine brukerbidrag - Ferdigbehandlet";

# Collections
$lang["editcollection"]="Rediger mappe";
$lang["resetarchivestatus"]="Tilbakestill arkivstatusen for alt materiale i mappen";
$lang["editallresources"]="Rediger alt materiale i mappen";
$lang["emailcollection"]="Send mappen som e-post";
$lang["collectionname"]="Mappenavn";
$lang["collectionid"]="mappe-ID";
$lang["removecollectionareyousure"]="Er du sikker på at du vil fjerne mappen fra listen din?";
$lang["managemycollections"]="Administrer mappe";
$lang["createnewcollection"]="Opprett ny mappe";
$lang["findpubliccollection"]="Finn en åpen mappe";
$lang["searchpubliccollections"]="Søk i åpne mapper";
$lang["addtomycollections"]="Legg til mine mapper";
$lang["action-addtocollection"]="Legg til mappe";
$lang["action-removefromcollection"]="Fjern fra mappe";
$lang["addtocollection"]="Legg til mappe";
$lang["cantmodifycollection"]="Du kan ikke endre denne mappen";
$lang["currentcollection"]="Aktiv mappe";
$lang["viewcollection"]="Vis mappe";
$lang["mycollection"]="Min mappe";
$lang["emailcollectionmessageexternal"]="har sendt deg en mappe med filer fra $applicationname."; # suffixed to user name e.g. "Fred has e-mailed you a collection.."
$lang["clicklinkviewcollection"]="Klikk på lenken nedenfor for å se mappen.";
$lang["zippedcollectiontextfile"]="Inkluder tekstfil med fil-/mappe-data.";
$lang["purgecollectionareyousure"]="Er du sikker på at du vil fjerne denne mappen OG SLETTE alle filene i det?";
$lang["collectionsdeleteempty"]="Slett tomme mapper";
$lang["collectionsdeleteemptyareyousure"]="Er du sikker på at du vil slette alle dine tomme mapper?";
$lang['hide_collection']="Ikke vis mappe";
$lang["showcollectionindropdown"]="Vis på mappebjelken";
$lang["view_shared_collections"]="Vis åpne mapper";
$lang['view_all_resources'] = 'Vis alle ressurser';
$lang['edit_all_resources'] = 'Rediger alle ressurser';

# Resource create / edit / view
$lang["upload"]="Registrer og last opp";
$lang["emailresource"]="Send materiale via e-post";
$lang["youfoundresource"]="fil"; # e.g. you found 1 resource
$lang["addtocurrentcollection"]="Legg til valgt mappe";
$lang["savethissearchtocollection"]="Lagre dette søket til mappen";
$lang["action-upload-to-collection"]="Last opp til denne mappen";
$lang['remove_custom_access_no_users_found'] = 'Ingen brukere eller grupper med spesialtilgang ble funnet';
$lang["resources_selected-1"]="1 ressurs valgt"; # 1 resource selected
$lang["resources_selected-2"]="%number ressurser valgt"; # e.g. 17 resources selected

# Resource status
$lang["status-2"]="Brukerbidrag: Venter på brukerendring";
$lang["status-1"]="Brukerbidrag: Venter på godkjenning" ;
$lang["status0"]="Ferdigbehandlet";

# Research request
$lang["copyexistingresources"]="Kopier materialet i et eksisterende mappe som svar på denne forespørselen";
$lang["typecollectionid"]="(Skriv mappe-ID nedenfor)";
$lang["researchrequestcompletemessage"]="Din materialforespørsel er besvart og resultatet er lagt til din 'Min mappe'-side.";

# Misc / global
$lang["action-viewmatchingresources"]="Vis lignende filer";
$lang["are_available-0"]="er tilgjengelig";
$lang["are_available-1"]="er tilgjengelig";
$lang["are_available-2"]="er tilgjengelig";
$lang["were_available-0"]="var tilgjengelig";
$lang["were_available-1"]="var tilgjengelig";
$lang["were_available-2"]="var tilgjengelig";

# Statistics
$lang["stat-addpubliccollection"]="Legg til den åpne mappen";
$lang["stat-addresourcetocollection"]="Legg til mappen";
$lang["stat-addsavedsearchtocollection"]="Lagre søket til mappen";
$lang["stat-addsavedsearchitemstocollection"]="Legg filer fra søket til mappen";
$lang["stat-e-mailedcollection"]="Mappe sendt via e-post";
$lang["stat-newcollection"]="Ny mappe";

# Admin - Bulk E-mails
$lang["emailhtml"]="Skru på HTML support - Eposten må da inneholde HTML kode";

# New for 1.3
$lang["savesearchitemstocollection"]="Lagre søkte filer til mappen";
$lang["removeallresourcesfromcollection"]="Fjern alle filene i denne mappen";
$lang["deleteallresourcesfromcollection"]="Slett alle filene i denne mappen";
$lang["deleteallsure"]="Er du sikker på at du vil SLETTE alle filene? Dette vil slette selve filene, ikke bare fjerne dem fra mappen.";
$lang["batchdonotaddcollection"]="(ikke legg til en mappe)";
$lang["collectionsthemes"]="Beslektede temaer og mapper";
$lang["recent"]="Nye bidrag";
$lang["downloadzip"]="Last ned mappen som en zip-fil";
$lang["confirmeditall"]="Er du sikker på at du vil lagre? Dette vil overskrive eksisterende verdier i de valgte feltene for alle filer i din nåværende mappe.";
$lang["confirmsubmitall"]="Er du sikker på at du vil sende alle inn til gjennomgang? Dette vil overskrive de eksisterende verdien(e) for de valgte feltene for alt materiell i din nåværende mappe og sende dem inn til gjennomgang.";
$lang["confirmunsubmitall"]="Er du sikker på at du ønsker å trekke tilbake alle fra gjennomgangsprosessen? Dette vil overskrive de eksisterende verdien(e) for de valgte feltene for alt materiell i din nåværende mappe og trekke dem fra gjennomgangsprosessen.";
$lang["confirmpublishall"]="Er du sikker på at du vil publisere? Dette vil Dette vil overskrive de eksisterende verdien(e) for de valgte feltene for alt materiell i din nåværende mappe og publisere alle for offentlig visning.";
$lang["confirmunpublishall"]="Er du sikker på at du vil avpublisere? Dette vil overskrive de eksisterende verdien(e) for de valgte feltene for alt materiell i din nåværende mappe og fjerne dem fra offentlig visning.";
$lang["collectiondeleteconfirm"]="Er du sikker på at du vil slette mappen?";

# New for 1.4
$lang["reorderresources"]="Omorganiser filene i mappen (klikk og dra)";
$lang["collectioncomments"]="mappe-kommentarer";
$lang["collectioncommentsinfo"]="Legg en kommentar til denne mappen for denne filen. Denne gjelder kun for denne mappen.";
$lang["addresourcebatchbrowser"]="Registrer og last opp nytt materiale";
$lang["addresourcebatchbrowserjava"]="Registrer og last opp nytt materiale (Java)";
$lang["relateallresources"]="Gjør filene i denne mappen beslektet";

# New for 1.5
$lang["noresourcesfound"]="Ingen ressurser funnet";

# New for 1.6
$lang["feedbacknocomments"]="Du har ikke skrevet noen kommentarer til filene i mappen.<br />Klikk på snakkeboblen ved hver fil for å legge til kommentarer.";
$lang["collectionfeedback"]="Tilbakemelding for mappe";
$lang["sharecollection"]="Del mappen";
$lang["collectionviewhover"]="Klikk for å se filene i denne mappen";
$lang["collectioncontacthover"]="Lag et oversiktsark med filene i denne mappen";
$lang["copyfromcollection"]="Kopier fra mappen";
$lang["donotcopycollection"]="Ikke kopier fra mappen";
$lang["resourcesincollection"]="filer i denne mappen"; # E.g. 3 resources in this collection
$lang["removefromcurrentcollection"]="Fjern fra denne mappen";
$lang["maxcollectionthumbsreached"]="Det er for mange filer i denne mappen til å vise miniatyrbilder. Miniatyrbildene vil nå bli skjult.";

# New for 1.7
$lang["confirmdeleteaccess"]="Er du sikker på at du vil slette denne tilgansnøkkelen? Brukere som har hatt tilgang via denne nøkkelen vil ikke lenger få tilgang til denne mappen.";
$lang["sharedcollectionaddwarning"]="Advarsel: Dette lybordet har blitt delt med eksterne brukere. Filen du har lagt til har nå blitt tilgjenelig for disse brukerne. Klikk på 'Dele' for å behandle den eksterne tilgangen til denne mappen.";

#MuseumsIT
$lang["intro-plupload"] = "Trykk \'Add files\' for finne frem til en eller flere filer. Trykk deretter \'Start upload\'.";
$lang["pluploader_warning"]="Nettleseren din støtter kanskje ikke veldig store filer.";
$lang["selectcollection"]="Velg Mappe";
$lang["n_recent"]="%qty Nylig registrerte brukerbidrag";
$lang["intro-batch_edit"] = "Vennligst fyll inn feltene som vil følge med filene du i neste steg skal laste opp. ";
$lang["starttypingkeyword"]="Enkeltord, flertall ubestemt form: Hester, Bygninger";
$lang["geographicsearch"]="Geografisk søk";
$lang["userpending"]="Brukerbidrag med status: Venter på godkjenning";
$lang["userpendingsubmission"]="Brukerbidrag med status: Venter på brukerendring";
$lang["upload-options"]="Alternativer for opplasting";
$lang["ownedbyyou"]="Eies av deg";
$lang["searchitemsdiskusage"]="Diskplass brukt av resultatene";
$lang["theme_home_promote"]="Vis som en kategori på hjemmesiden?";
$lang["search-mode"]="Søk etter...";
$lang["resources-all-types"]="Alle typer";
$lang["nomatchingresults"]="Ingen treff";
$lang["matchingresults"]="treff"; # e.g. 17 matching results=======
$lang["resourcetype-audio-2"]="Lyder";
$lang["resourcetype-photo-2"]="Bilder";
$lang["resourcetype-video-2"]="Videoer";
$lang["resourcetype-document-2"]="Dokumenter";
$lang["allresourcessearchbar"]="Alle filer";
$lang["allcollectionssearchbar"]="Alle mapper";
$lang["continuetoresults"]="Fortsett til resultater";
$lang["allresources"]="Alle filer";
$lang["resourcetypes-no_collections"] = "Alle %resourcetypes%"; # Use %RESOURCETYPES%, %resourcetypes% or %Resourcetypes% as a placeholder. The placeholder will be replaced with the resourcetype in plural (or $lang["all-resourcetypes"]), using the same case. E.g. "All %resourcetypes%" -> "All photos"
$lang["collections-1"] = "(<strong>1</strong> Mappe)";
$lang["collections-2"] = "(<strong>%d</strong> Mapper med <strong>%d</strong> filer)"; # %number will be replaced, e.g. 3 Collections
$lang["total-collections-0"] = "<strong>Totalt: 0</strong> Mapper";
$lang["total-collections-1"] = "<strong>Totalt: 1</strong> Mappe";
$lang["total-collections-2"] = "<strong>Totalt: %number</strong> Mapper"; # %number will be replaced, e.g. Total: 5 Collections
$lang["owned_by_you-0"] = "(<strong>0</strong> Eies av deg)";
$lang["owned_by_you-1"] = "(<strong>1</strong> Eies av deg)";
$lang["owned_by_you-2"] = "(<strong>%mynumber</strong> Eies av deg)"; # %mynumber will be replaced, e.g. (2 owned by you)
$lang['actions'] = 'Handlinger';
$lang["action-log"]="Vis logg";
$lang["emptycollection"] = "Fjern materiale ";
$lang["share-resource"]="Del materialet";
$lang["maximise"]="Maksimer";
$lang["downloadmetadata"]="Last ned metadata";
$lang["downloadingmetadata"]="Laster ned metadata";
$lang["share-resource"]="Del materiale";
$lang["deletedresource"] = "Slettet materiale";
$lang["deletedresources"] = "Slettet materiale";

$lang["user-preferences"]="Mine innstillinger";
$lang["geographicsearch_help"]="Dra og slipp for å velge et område.";
$lang["geodragmode"]="Dra og slipp modus";
$lang["geodragmodeareaselect"]="velg søkeområde";
$lang["geodragmodepan"]="panorer";
$lang["geographicsearchresults"]="Resultater for geografisk søk";
$lang["geodragmodearea"]="merk posisjon";

$lang['collection_disk_usage'] = 'Diskplass brukt av alle ressurser';
$lang["shared_collections"]="Åpne mapper";
$lang["managecollectionslink"]="Administrer mapper";
$lang["collection-order"] = "Mapperekkefølge";

# Collection log - actions
$lang["collectionlog"]="Mappe-logg";
$lang["collectionlog-S"]="Delt mappe med "; //  + notes field
$lang["collectionlog-E"]="Sendt mappe med e-post til ";//  + notes field
$lang["collectionlog-T"]="Sluttet å dele mappe med ";//  + notes field
$lang["collectionlog-X"]="Mappe slettet";
$lang["viewuncollectedresources"]="Vis filer som ikke er brukt i noen mappe";

# Collection requesting
$lang["requestcollection"]="Mappe-forespørsel";

# Video Playlist
$lang["restrictedsharecollection"]="Du har begrenset tilgang til en eller flere filer i denne mappen og deling er derfor ikke tilatt.";
$lang["collection"]="Mappe";
$lang["mycollection_notpublic"]="Du kan ikke gjøre om 'Min mappe' til en åpen mappe eller kategori. Vennligst lag en ny mappe for dette formålet.";

##  Translations for standard log entries
$lang["cannotshareemptycollection"]="Denne mappen er tom og kan ikke deles.";
$lang["requestdeclinedmail"]="Beklager, din forespørsel etter materialet i mappen nedenfor har blitt avslått.";

#Location Data
$lang['location'] = 'Lokasjon';
$lang['mapzoom'] = 'Kart zoom';

$lang["publiccollections"]="Åpne mapppe";
$lang["accountemailalreadyexists"]="En konto med Epostadressen eksisterer allerede";
$lang["backtothemes"]="Tilbake til kategorier";
$lang["downloadreport"]="Last ned rapport";


#Bug Report Page
$lang["associatedcollections"]="Assosierte mapper";
$lang["uncollectedresources"]="Filer som ikke opptrer i mapper";

$lang["requestaddedtocollection"]="Dette materialet har blitt lagt til den aktive mappen. Du kan bestille filene i mappen din ved å klikke på \'Bestill alle\' i mappe-bjelken nedenfor.";

# Reports
# Report names (for the default reports)
$lang["report-resources_added_to_collection"]="Filer lagt til mappen";

# Permissions Manager
$lang["themes_and_collections"]="Kategorier / mapper";
$lang["enable_bottom_collection_bar"]="Skru på mappe-bjelken nederst i arbeidsvinduet ('mapper')";
$lang["can_publish_collections_as_themes"]="Kan publisere mapper som Åpne mapper";
$lang["nodownloadcollection"]="Du har ikke tilgang til å laste ned noe av materialet i denne mappen.";

# CSV Export of Search results
$lang['csvExportResultsMetadata'] = 'CSV Eksport - Resultater metadata';
$lang['csvAddMetadataCSVToArchive'] = 'Inkluder metadata CSVfil til arkivet?';


##########################################################################################
# Non page-specific items that need to be merged above when system admin project completed
##########################################################################################
$lang["home__help"]="Få hjelp med Resourcespace.";
$lang["home__mycollections"]="Organiser, samarbeid og del ditt materiale.";
$lang["home__restrictedtitle"]="Velkommen til ResourceSpace [ver]";
$lang["home__themes"]="De beste bidragene, sortert i grupper .";
$lang["home__welcometext"]="Velkommen til Museets DAM";
$lang["home__welcometitle"]="Velkommen til Resourcespace [ver]";
$lang["all__searchpanel"]="Søk ved hjelp av metadata registrert på materialet";

$lang["collection_manage__introtext"]="Organiser og hold ved like arbeidet ditt ved å gruppere bilder. Lag 'mapper' som passer din arbeidsmåte. Alle mappene i listen din finnes under 'Mine mapper' nederst på skjermen";
$lang["collection_manage__newcollection"]="For å lage en ny mappe, skriv inn et kortnavn.";
$lang["collection_manage__findpublic"]="Åpne mapper er samlinger av media gjort tilgjengelig av alle brukere av systemet. Skriv inn en collection ID, hele/deler av mappenavnet eller brukernavn for å finne åpne mapper. Legg dem til i din liste over mapper for å få tilgang til mediene.";
$lang["themes__introtext"]="Kategorier er samlinger av materiale som har blitt valgt ut av administratorer for å vise et eksempel på hva som finnes i samlingene.";
$lang["themes__findpublic"]="Delte mapper er en samling av bilder/media som har blitt delt av andre brukere av systemet.";
$lang["collection_public__introtext"]="Åpne mapper er laget av andre brukere av systemet og delt til alle.";
$lang["search_advanced__introtext"]="<strong>Søketips</strong><br />En seksjon du lar være blank eller ikke krysset av vil returnere ALLE termene for seksjonen. Hvis du for eksempel lar lokasjon være tom vil du få resultater fra alle lokasjoner. Hvis du skriver inn en lokasjon vil resultatet returnere kun denne lokasjonen.";
$lang["resource_email__introtext"]="Du kan dele dette materialet med andre via epost. En lenke blir automatisk sendt ut. Du kan også skrive inn en beskjed i eposten.";
$lang["internal_share_grant_access"]="Gi åpen tilgang til valgte interne brukere?";
$lang["team_resource__introtext"]="Her kan du gå igjennom materiale fra brukerne, eller laste opp nytt materiale via batch eller enkeltfiler.";
$lang["batch_replace_filename_intro"]="For å erstatte en batch av ressurser kan du laste opp filer med filnavn som matcher resource ID i Resourcespace. Alternativt kan du velge et metadata felt som inneholder filnavnet. Systemet vil da se etter en match med det opplastede filnavnet for å bestemme hvilke som skal erstattes";
$lang["batch_replace_use_resourceid"]="Match filnavn med Material ID";
$lang["batch_replace_filename_field_select"]="Spesifiser feltet som inneholder filnavn.";
$lang["team_copy__introtext"]="Skriv inn Material-ID for ressursen du vil kopiere. Bare metadataene vil bli kopiert - Selve bildet-mediet vil ikke bli kopiert.";
$lang["team_archive__introtext"]="For å redigere individuelle ressurser, søk etter ressursen og trykk rediger under selve ressursen. Alle ressurser som er klare til å bli arkivert listes ut under Vis materiale til arkivering.";
$lang["team_user__introtext"]="Bruk denne siden for å legge til, endre og fjerne brukere.";
$lang["team_stats__introtext"]="Statistikken er basert på levende data fra brukerne. Bruk nedtrekksmenyene for å ta et utvalg.";
$lang["team_report__introtext"]="Vennligst velg en rapport og et tidsintervall. Rapporten kan åpnes i Excel.";
$lang["contribute__introtext"]="Du kan legge inn dine egne bidrag. Når du laster opp bidrag vil de komme i kategorien 'venter på brukerendring'. Når du er fornøyd med bidraget kan du endre status på materialet til 'venter på godkjenning'. En administrator vil da publisere bidraget ditt.";
$lang['mymessages_introtext'] = "Du kan sette opp hvilke meldinger som skal dukke opp her ved å endre dine kontoinnstillinger.";

/*
 * Start - User Dash Strings
 */
$lang["savethissearchtodash"]="Lagre til skrivebordflis";
$lang["pushtoallusers"]="Legg ut skrivebordsflis til alle brukere?";
$lang["createnewdashtile"]="Lag ny skrivebordsflis";
$lang["specialdashtiles"]="Spesiell skrivebordsflis";
$lang["editdashtile"]="Endre skrivebordsflis";
$lang["createdashtilefreetext"]="Lag skrivebordsflis med bare tekst";
$lang["enterdefaultorderby"]="Skriv inn standard posisjonsnummer";
$lang["dashtiletitle"]="Tittel";
$lang["dashtiletext"]="Tekst";
$lang["dashtilelink"]="Tittel mållenke";
$lang["nodashtilefound"]="Ingen skrivebordsflis funnet";
$lang["existingdashtilefound"]="Skrivebordsflisen finnes allerede.";
$lang["invaliddashtile"]="Feil skrivebordsflisreferanse";
$lang["dashtilestyle"]="Skrivebordsflis stil";
$lang["returntopreviouspage"]="Returner til forrige side";
$lang["showresourcecount"]="Vis antall objekter?";
$lang["tilebin"]="Fjern";
$lang["last"]="Siste";
$lang["managedefaultdash"]="Organiser alle fliser";
$lang["dashtile"]="Skrivebordsflis";
$lang["manage_own_dash"]="Mitt skrivebord";
$lang["manage_all_dash_h"]="Endre standard skrivebord / alle skrivebordsfliser (Du trenger h tillatelse)";
$lang["manage_all_dash"]="Endre standard skrivebord / alle skrivebordsfliser";
$lang["dashtiledeleteaction"]="Hvilke sletteoperasjon vil du foreta?";
$lang["confirmdashtiledelete"]="Slett flis fra mitt skrivebord";
$lang["dashtiledeleteusertile"]="Denne skrivebordsflisen vil bli permanent slettet";
$lang["confirmdefaultdashtiledelete"]="Slett flis for alle brukere";
$lang["dashtiledelete"]="Slett skrivebordsflis";
$lang["error-missingtileheightorwidth"]="Mangler høyde eller bredde";
$lang["dashtileimage"]="Skrivebordsflis bilde";
$lang["dashtilesmalldevice"]="Dra og slipp vil ikke fungere på enheter med liten skjerm";
$lang["dashtileshow"]="Vis tittel";
$lang["dasheditmodifytiles"]="Endre skrivebordsfliser som er tilgjengelig";
$lang['confirmdeleteconfigtile']="Denne tittelen er styrt av konfigurasjon. For å slette denne permanent, skru av den rette konfigurasjonen og prøv igjen.";
$lang["error-dashactionmissing"]="No action or invalid data was submitted to this page. No tile template available to build. <br />Please return to this page from a suitable creation / edit link";
$lang["dasheditchangeall_users"]="Turning off this setting will not remove this tile from all dashes, you must do this from manage all user tiles. However, new users will no longer receive this tile on their dash.";
$lang["dashtilevisitlink"]="Gå til mållenken";
$lang["alluserprebuiltdashtiles"]="Lag forhåndsinnstilte skrivebordsfliser (Legges til for alle brukere)";
$lang["manageowndashinto"]="Rediger skrivebordsfliser for ditt skrivebord";

/* Create Config dash tile link descriptions (text) */
$lang["createdashtilependingsubmission"]="Brukerbidrag med Status: Venter på brukerendring (Vises ikke hvis ingen eksisterer)";
$lang["createdashtilependingreview"]="Brukerbidrag med Status: Venter på godkjenning (Vises ikke hvis ingen eksisterer)";
$lang["createdashtilethemeselector"]="Mapper med merke for en spesiell kategori";
$lang["createdashtilethemes"]="Mapper på forsiden";
$lang["createdashtilemycollections"]="Mine mapper (brukerspesifikke)";
$lang["createdashtileadvancedsearch"]="Avansert søkelenke";
$lang["createdashtilemycontributions"]="Mine bidrag (brukerspesifikk)";
$lang["createdashtilehelpandadvice"]="Hjelpelenke";
$lang["createdashtileuserupload"]="Last opp og rediger flis (brukerspesifikk)";
#Tile style strings
$lang["tile_thmbs"]="Enkel";
$lang["tile_multi"]="Flere";
$lang["tile_blank"]="Blank";
$lang["tile_ftxt"]="Bare tekst";
/* * End - User Dash Strings * */

/* My Account Strings */
$lang["myaccount"]="Min konto";
$lang["userpreferences"]="Mine innstillinger";
$lang["modifyuserpreferencesintro"]="Innstillinger på denne siden gir deg mulighet til å skreddersy utseende og funksjonalitet for din Resourcespace konto.";

/* User preferences*/
$lang['userpreference_colourtheme'] = 'Fargetema';
$lang["userpreferencecolourtheme"]="Grensesnittfarge tema";
$lang['userpreference_user_interface'] = 'Grensesnitt';
$lang['userpreference_enable_option'] = 'Aktiver';
$lang['userpreference_disable_option'] = 'Deaktiver';
$lang['userpreference_default_sort_label'] = 'Standard sortering';
$lang['userpreference_default_perpage_label'] = 'Standard per side';
$lang['userpreference_default_display_label'] = 'Standard visning';
$lang['userpreference_use_checkboxes_for_selection_label'] = 'Bruk avkrysningsbokser for å opprette mapper';
$lang['userpreference_resource_view_modal_label'] = 'Ny materialvisning';
$lang['userpreference_thumbs_default_label'] = 'Standard mappevisning i bjelke';
$lang['userpreference_basic_simple_search_label'] = 'Standard enkelt søk';
$lang['userpreference_cc_me_label'] = 'CC til meg selv når jeg sender epost';
$lang['userpreference_email_me_label'] = 'Send meg epost i stedet for system meldinger';
$lang['userpreference_email_digest_label'] = 'Send meg en daglig sammenfattet epost, istedet for mange';
$lang['userpreference_system_management_notifications'] = "Send meg beskjeder om viktige systemhendelser, som liten diskplass";
$lang['userpreference_user_management_notifications'] = "Send meg administrasjonsmeldinger, som forespørsler om å opprette brukere";
$lang['userpreference_resource_access_notifications'] = "Send meg meldinger om forespørsler om tilgang til materiale";
$lang['userpreference_resource_notifications'] = "Send meg meldinger om ressursbehandling. Som for eksempel endriger av metadata.";
$lang['user_pref_daily_digest_mark_read'] = "Merk meldinger som lest når jeg har blitt tilsendt oppsummeringen på epost";
$lang['user_pref_show_notifications'] = "Vis meg systemmeldinger når de blir mottatt. Hvis deaktivert vil telleren fortsatt vise antall systemmeldinger";
$lang['user_pref_daily_digest'] = "Send meg en daglig epost med alle uleste notiser fra de siste 24 timer";


/* Messaging */
$lang["mymessages"]="Mine meldinger";
$lang["screen"]="Skjerm";
$lang["mymessages_markread"]="Merk lest";
$lang["mymessages_markunread"]="Merk ulest";
$lang["mymessages_markallread"]="Merk alle som lest";
$lang["mymessages_youhavenomessages"]="Du har ingen nye meldinger";
$lang["message_type"]="Meldingstype";
$lang["message_url"]="Meldingslenke";
$lang["sendbulkmessage"]="Send samlemelding";
$lang["message_sent"]="Melding sendt";
?>
